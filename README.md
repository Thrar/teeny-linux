Build a minimalistic Linux distribution for QEMU
================================================

Taken from [this blog post by Mitchel Humpherys](http://mgalgs.github.io/2015/05/16/how-to-build-a-custom-linux-kernel-for-qemu-2015-edition.html).

This folder contains scripts related to building and running micro-VMs with QEMU, i.e. VMs with a minimalistic kernel and userspace. The userspace is provided by Busybox.


## Set up

Download both Busybox and the Linux kernel:
```shell
curl https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.6.6.tar.xz | tar xJf -
curl https://busybox.net/downloads/busybox-1.24.2.tar.bz2 | tar xjf -
```

You might want to use newer versions of both, but my provided ".config" files are known to work with Busybox 1.24.2 and Linux 4.6.6.

We will generate all the compiled stuff under the same directory "obj":
```shell
mkdir obj
```

## Busybox userspace

First, generate a default Busybox configuration and setup:
```shell
cd busybox-1.24.2
make O=../obj/busybox defconfig
```

Now, two choices:
 * use the config file "config_busybox" I provided, by copying it into "obj/busybox" under the name ".config";
 * follow the instructions in [the source blog post](http://mgalgs.github.io/2015/05/16/how-to-build-a-custom-linux-kernel-for-qemu-2015-edition.html) if you want to change more things.

Then, actually build Busybox:
```shell
cd obj/busybox
make # maybe use more jobs: -j4 or more
make install
```

Busybox is _statically linked,_ so we go with preparing the files to make an initramfs from them:
```shell
mkdir ../initramfs
cd initramfs
mkdir -pv {bin,sbin,etc,proc,sys,usr/{bin,sbin}}
cp -av ../busybox/_install/* .
```

Mitchel makes a warning about how **a lot is missing** and that can break some applications (such as a missing "/etc/passwd", etc.). It is enough for now though, except one piece: an **init script.**

I provide one, which is functionally the same as Mitchel's one. It includes commented lines that would mount supplementary stuff and run the DHCP client or the HTTP server.

Finally, build the initramfs using my script (which is basically the command Mitchel runs):
```shell
cd ../..
./makeinitramfs.sh
```

We're done with the user space, which is provided as the initramfs; now on to the kernel!

## Linux minimalistic kernel

First, generate a default Linux configuration and setup:
```shell
cd linux-4.6.6
make O=../obj/linux allnoconfig
```

As you can see from `allnoconfig`, we are going for the _smallest configuration_ possible.

Now, just like with Busybox, you can:
 * use the config file "config_linux" I provided, by copying it into "obj/linux" under the name ".config";
 * follow the instructions in [the source blog post](http://mgalgs.github.io/2015/05/16/how-to-build-a-custom-linux-kernel-for-qemu-2015-edition.html) if you want to change more things.

My configuration only includes the minimal set of options as given by Mitchel, as well as the ones needed to make the kernel a **KVM-enabled guest.**

Then, actually build Linux:
```shell
make O=../obj/linux # maybe use more jobs: -j4 or more
```

Thus we have our minimalistic kernel!

## Create a minimalistic VM with QEMU/KVM

The basic QEMU command line, as given by Mitchel, is the following (run from the top-level working directory):
```shell
qemu-system-x86_64 \
    -kernel obj/linux/arch/x86_64/boot/bzImage \
    -initrd obj/initramfs-busybox-x86.cpio.gz \
    -nographic -append "console=ttyS0" -enable-kvm
```

Important flags are `-kernel` and `-initrd`, which tells QEMU where to find respectively, the kernel image and the initramfs (`initrd` for "initial RAM disk").

I also like to add `-cpu host` to emulate the exact CPU host.

At any rate, executing this command will put you in front of a shell in your lighter-than-air VM!

## Bonus: run a lot of VMs

My usage of this micro-VM was to create _a lot_ of instances of it on my computer (I went up to 200 if memory serves well, with 16Go of RAM). The script "runalot.sh" will do so.

Execute it and pass it a number of instances to create, and let it work.

Some settings can be changed in the first section of the script, have a look at the comments.

## Bonus: minimalistic VM with Apache HTTP server

The provided init script has lines commented out, that can start an Apache server on boot. As it is, the pages are served from the folder "/www", which is provided by a virtual disk mounted by the init script.

`qemu-img` can be used to create an image file for this purpose.

This is a good way of communicating information from the VM to the outer world.

## Licensing information

As far as the code in this repository is concerned, you can do [what the fuck you want to](http://www.wtfpl.net/about/):

```
DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
            Version 2, December 2004

Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

    DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.
```

The information used to create all of this comes from [this blog post by Mitchel Humpherys](http://mgalgs.github.io/2015/05/16/how-to-build-a-custom-linux-kernel-for-qemu-2015-edition.html), and is its own propriety.
