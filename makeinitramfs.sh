#!/bin/sh

# Build an initramfs from the specified files.

BUSYBOX_INITRAMFS="./obj/initramfs/"
INITRAMFS_CPIOGZ="./obj/initramfs-busybox-x86_64.cpio.gz"

find "$BUSYBOX_INITRAMFS" -printf '%P\0' | cpio -o --null -D "$BUSYBOX_INITRAMFS" --format=newc | gzip -9 > "$INITRAMFS_CPIOGZ"
