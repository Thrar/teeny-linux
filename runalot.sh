#!/bin/bash

# Run a great number of "teeny Linux" VMs, i.e. with minimal kernel and userspace.
# Variable `drive` may be used to give them a virtual disk.
# VM run by this script are daemonized, and have no display. If you don't enable
# a network interface (default setting), you won't be able to communicate with
# them.

kernel="obj/linux-x86_64-allnoconfig/arch/x86_64/boot/bzImage"
initrd="obj/initramfs-busybox-x86_64.cpio.gz"
qemuOptions="-enable-kvm -display none -daemonize -cpu host"

append=""

# Real, host network interface to use as bridge
# Leave empty to disable networking (VM will have no network interface)
netItf=""
# netItf="br0"
# Base value of VMs' MAC addresses
# Must include exactly 5 bytes, and end with ":")
# Will be concatenated with the ID of the instance, under hexadecimal form
macAddr=""
# macAddr="52:54:BA:C0:08:"
# Virtual drive for the VM
# Leave empty to give none
drive=""
#drive="-drive file=www.img,format=raw,if=virtio,cache=none"

##### END OF CONFIGURATION SECTION #####

nbInst="$1"
if [ -z "$1" ]; then
    echo "Required argument: number of instances"
    exit 2
fi

qemu="qemu-system-x86_64"
if [ -n "$append" ]; then
    append="-append $append"
fi

net=""
# To be completed with the MAC addr
if [ -n "$netItf" ]; then
    net="-net bridge,vlan=0,br=$netItf -net nic,vlan=0,model=virtio,macaddr="
fi

for i in $(eval echo "{1..$nbInst}"); do
    echo "$i/$nbInst"
    thisMACAddr=""
    if [ -n "$net" ]; then
        thisMACAddr="$macAddr$(echo "obase=16; $i" | bc)"
    fi
    $qemu -kernel $kernel -initrd $initrd $qemuOptions $append $net$thisMACAddr $drive
done
